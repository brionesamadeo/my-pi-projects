from time import sleep
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate
import RPi.GPIO as GPIO
# Disable any in use warnings
GPIO.setwarnings(False)
GPIO.cleanup()
# Set up the GPIO library to use Raspberry Pi
# board pin numbers
GPIO.setmode(GPIO.BOARD)
 # Set up the pin numbers we are using for each LED
RED=22
AMBER=24
GREEN=26
BUZZER=18
LIGHT=16

lcd = Adafruit_CharLCDPlate()

# Set Pin 11, 16 and 7 on the GPIO header to act as an output
GPIO.setup(RED,GPIO.OUT)
GPIO.setup(AMBER,GPIO.OUT)
GPIO.setup(GREEN,GPIO.OUT)
GPIO.setup(BUZZER,GPIO.OUT)
GPIO.setup(LIGHT,GPIO.OUT)

print ("Mode is now set to output command")

# Reset all the lights to off
lcd.clear()
GPIO.output(RED,GPIO.LOW)
GPIO.output(AMBER,GPIO.LOW)
GPIO.output(GREEN,GPIO.LOW)
GPIO.output(BUZZER,GPIO.LOW)
GPIO.output(LIGHT,GPIO.LOW)
print("GPIO off")
lcd.backlight(lcd.OFF)
print("LCD Screen Off")

exit()