#!/usr/bin/python
# First we need to import the libraries that
# we need
# Import the time library so that we can make
# the program pause for a fixed amount of time
import time
from time import sleep
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate
import RPi.GPIO as GPIO
# Disable any in use warnings
GPIO.setwarnings(False)
GPIO.cleanup()

# Set up the GPIO library to use Raspberry Pi
# board pin numbers
GPIO.setmode(GPIO.BOARD)
RED=22
AMBER=24
GREEN=26
WAIT=1

lcd = Adafruit_CharLCDPlate()

# Clear display and show greeting, pause 1 sec
lcd.clear()
lcd.message("Adafruit RGB LCD\nPlate w/Keypad!")

GPIO.setup(RED,GPIO.OUT)
GPIO.setup(AMBER,GPIO.OUT)
GPIO.setup(GREEN,GPIO.OUT)

# Reset all the lights to off
GPIO.output(RED,GPIO.LOW)
GPIO.output(AMBER,GPIO.LOW)
GPIO.output(GREEN,GPIO.LOW)

# This loop runs forever and flashes the LED
while True:
        # Turn on the red LED
        GPIO.output(RED,GPIO.HIGH)
        lcd.clear()
        lcd.backlight(lcd.RED)
        lcd.message("RED!")
        time.sleep(WAIT)

        # Turn on the amber LED
        # GPIO.output(AMBER,GPIO.HIGH)
        # print "Red and Amber"
        # time.sleep(WAIT)
        # Turn off the amber LED
        # GPIO.output(AMBER,GPIO.LOW)

        # Turn off the red LED
        GPIO.output(RED,GPIO.LOW)
        # Turn on the green LED
        GPIO.output(GREEN,GPIO.HIGH)
        lcd.clear()
        lcd.backlight(lcd.GREEN)
        lcd.message("GREEN!")
        time.sleep(WAIT)
        # Turn off the green LED
        GPIO.output(GREEN,GPIO.LOW)
        # Turn on the amber LED
        lcd.clear()
        lcd.backlight(lcd.YELLOW)
        lcd.message("YELLOW!")
        GPIO.output(AMBER,GPIO.HIGH)
        time.sleep(WAIT)
        GPIO.output(AMBER,GPIO.LOW)