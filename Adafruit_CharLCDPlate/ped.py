import time
from time import sleep
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate
import RPi.GPIO as GPIO
# Disable any in use warnings
GPIO.setwarnings(False)
GPIO.cleanup()
# Set up the GPIO library to use Raspberry Pi
# board pin numbers
GPIO.setmode(GPIO.BOARD)
 # Set up the pin numbers we are using for each LED
RED=22
AMBER=24
GREEN=26
MINILIGHTS1=16
BUZZER=18

LIGHT=16
WAIT=1

# Wait times
RED_T=5
YELLOW_T=6
GREEN_T=1

lcd = Adafruit_CharLCDPlate()
lcd.backlight(lcd.GREEN)


# Set Pin 11, 16 and 7 on the GPIO header to act as an output
GPIO.setup(RED,GPIO.OUT)
GPIO.setup(AMBER,GPIO.OUT)
GPIO.setup(GREEN,GPIO.OUT)
GPIO.setup(MINILIGHTS1,GPIO.OUT)
GPIO.setup(BUZZER,GPIO.OUT)

# Set up pin 22 (SWITCH) to act as an input
# GPIO.setup(SWITCH,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)

# Reset all the lights to off
GPIO.output(RED,GPIO.LOW)
GPIO.output(AMBER,GPIO.LOW)
GPIO.output(GREEN,GPIO.LOW)
GPIO.output(MINILIGHTS1,GPIO.LOW)
GPIO.output(BUZZER,GPIO.LOW)

# This loop runs forever and runs the traffic lights sequence
while True:
    # Turn on the green LED
    GPIO.output(GREEN,GPIO.HIGH)
    ButtonPressed = False

    # Wait until a pedestrian presses the switch
    lcd.clear()
    lcd.backlight(lcd.GREEN)
    lcd.message("GREEN!     Press\n    Right Button")
    while not lcd.buttonPressed(lcd.RIGHT):
        # Wait for 2 seconds
        #time.sleep(GREEN_T)
        ButtonPfdfdssed = 1
    
    GPIO.output(MINILIGHTS1,GPIO.HIGH)
    lcd.clear()
    lcd.backlight(lcd.VIOLET)
    lcd.message("Button Pressed\nPlease wait - 2")
    time.sleep(1)
    GPIO.output(MINILIGHTS1,GPIO.LOW)
    lcd.clear()
    lcd.message("Button Pressed\nPlease wait - 1")
    time.sleep(1)
    
    # Turn off the green LED
    GPIO.output(GREEN,GPIO.LOW)
    # Turn on the amber LED
    GPIO.output(AMBER,GPIO.HIGH)
    
    lcd.clear()
    lcd.backlight(lcd.YELLOW)
    lcd.message("YELLOW!\n[=___________] 6")
    time.sleep(1)
    lcd.clear()
    lcd.message("YELLOW!\n[===_________] 5")
    time.sleep(1)
    lcd.clear()
    lcd.message("YELLOW!\n[=====_______] 4")
    time.sleep(1)
    lcd.clear()
    lcd.message("YELLOW!\n[=======_____] 3")
    time.sleep(1)
    lcd.clear()
    lcd.message("YELLOW!\n[=========___] 2")
    time.sleep(1)
    lcd.clear()
    lcd.message("YELLOW!\n[===========_] 1")
    time.sleep(1)
    
    GPIO.output(AMBER,GPIO.LOW)
    # Turn on the red LED
    GPIO.output(RED,GPIO.HIGH)
    lcd.clear()
    lcd.backlight(lcd.RED)
    lcd.message("RED!")
    # Wait for 4 seconds
    time.sleep(RED_T)
    # Turn off the red LED
    GPIO.output(RED,GPIO.LOW)

# End of code    
