#!/usr/bin/python

from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate
from subprocess import * 
from time import sleep, strftime
from datetime import datetime
import RPi.GPIO as GPIO

# Disable any in use warnings
GPIO.setwarnings(False)
GPIO.cleanup()

# Set up the GPIO library to use Raspberry Pi
# board pin numbers
GPIO.setmode(GPIO.BOARD)
RED=22
AMBER=24
GREEN=26
MINILIGHTS1=16
BUZZER=18

lcd = Adafruit_CharLCDPlate()
lcd.backlight(lcd.GREEN)
lcd.clear()

GPIO.setup(RED,GPIO.OUT)
GPIO.setup(AMBER,GPIO.OUT)
GPIO.setup(GREEN,GPIO.OUT)
GPIO.setup(MINILIGHTS1,GPIO.OUT)
GPIO.setup(BUZZER,GPIO.OUT)

# Reset all the lights to off
GPIO.output(RED,GPIO.LOW)
GPIO.output(AMBER,GPIO.LOW)
GPIO.output(GREEN,GPIO.LOW)
GPIO.output(BUZZER,GPIO.LOW)
GPIO.setup(MINILIGHTS1,GPIO.LOW)

cmd = "ip addr show wlan0 | grep inet | awk '{print $2}' | cut -d/ -f1"

lcd.begin(16,1)

def run_cmd(cmd):
        p = Popen(cmd, shell=True, stdout=PIPE)
        output = p.communicate()[0]
        return output
        

ipaddr = run_cmd(cmd)
GPIO.output(RED,GPIO.HIGH)
lcd.clear()
lcd.message(' Welcome to Pi!\n')
lcd.message('IP %s' % ( ipaddr ) )
GPIO.output(BUZZER,GPIO.HIGH)
GPIO.output(AMBER,GPIO.HIGH)
sleep(.5)
GPIO.output(BUZZER,GPIO.LOW)
GPIO.output(AMBER,GPIO.LOW)
sleep(.5)
GPIO.output(BUZZER,GPIO.HIGH)
GPIO.output(RED,GPIO.LOW)
GPIO.output(AMBER,GPIO.HIGH)
sleep(.5)
GPIO.output(BUZZER,GPIO.LOW)
GPIO.output(AMBER,GPIO.LOW)
GPIO.output(GREEN,GPIO.HIGH)
sleep(.5)
lcd.clear()
lcd.message("Let's Have fun!\n")
lcd.message('IP %s' % ( ipaddr ) )
sleep(5)
lcd.clear()
lcd.message('And Here We Go!' )
sleep(2)
GPIO.output(GREEN,GPIO.LOW)
GPIO.output(BUZZER,GPIO.LOW)
lcd = Adafruit_CharLCDPlate()
lcd.backlight(lcd.OFF)
lcd.clear()
exit