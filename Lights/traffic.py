# First we need to import the libraries that we need\
# Import the time library so that we can make
# the program pause for a fixed amount of time
import time

import RPi.GPIO as GPIO
# Now we need to set-up the General Purpose
# Input-Ouput (GPIO) pins
# Clear the current set-up so that we can
# start from scratch
# Set up the GPIO library to use Raspberry Pi
# board pin numbers
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

GPIO.setup(22, GPIO.OUT)
GPIO.setup(24, GPIO.OUT)
GPIO.setup(7, GPIO.OUT) ## Setup GPIO Pin 7 to OUT

# This loop runs forever and flashes the LED
while True:
  # Turn on the red LED
  GPIO.output(22,True)
# Wait for 2 seconds
time.sleep(2)
# Turn on the yellow LED
GPIO.output(24,True)
# Wait for 2 seconds
time.sleep(2)
# Turn off the yellow LED
GPIO.output(24,False)
# Turn off the red LED
GPIO.output(22,False)
# Turn on the green LED
GPIO.output(7,True)
# Wait for 2 seconds
time.sleep(2)
# Turn off the green LED
GPIO.output(7,False)
# Turn on the yellow LED
GPIO.output(24,True)
# Wait for 2 seconds
time.sleep(2)